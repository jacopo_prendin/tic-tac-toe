
CPP=g++
FLAGS=-std=c++17 -g
:-Wnonportable-include-path

player.o:
	$(CPP) $(FLAGS) -c player/player.cpp

consoleui.o:
	$(CPP) $(FLAGS) -c player/consoleui.cpp

lai.o:
	$(CPP) $(FLAGS) -c player/lai.cpp

tictactoe.o:
	$(CPP) $(FLAGS) -c tictactoe.cpp

all: player.o lai.o consoleui.o tictactoe.o
	$(CPP) $(FLAGS) player.o lai.o consoleui.o tictactoe.o -o tictactoe main.cpp

#------------------------------------ Tests ------------------------------------
testconsoleui: player.o consoleui.o
	$(CPP) $(FLAGS) player.o consoleui.o tests/test_consoleui.cpp -o testconsoleui

testlearningai: player.o lai.o
	$(CPP) $(FLAGS) player.o lai.o tests/test_learningai.cpp -o testlearningai

clean:
	rm -f tictactoe
	rm -f *.o
	rm -f testlearningai
	rm -f testconsoleui
