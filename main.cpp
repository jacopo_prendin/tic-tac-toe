
#include <string.h> // for memset
#include <iostream>

#include "tictactoe.hpp"
#include "player/consoleui.hpp"
#include "player/lai.hpp"


int main(int argc, char** argv){

  /*
   * this demonstration game will use a human player and simple learning
   * alghoritm
   */
  j33matrix board = new uint8_t*[3];
  for (int i=0; i<3; i++) {
    board[i] = new uint8_t[3];
    memset(board[i], 0, 3);
  }
  
  LearningAI cpu(board);
  ConsoleUI user(1,"Human Player",board,'O','X');

  TicTacToe ttt(board, &user, &cpu);
  bool playing = true;
  
  while (playing) {
    // initialize match
    ttt.reset();

    /*
     * enter the match loop. It continues until the TicTacToe object detects
     * a winner or a draw-game
     */
    while ( ttt.match_running() ) {
      ttt.manage_player_next_move();
    }

    /* who won the match? */
    if (!ttt.is_draw_game()) {
      std::cout<<"--- Winner ---";
      std::cout<<ttt.get_winner()<<std::endl;

      /* hey AI, learn from your experience */
      if (cpu.get_name() == ttt.get_winner())
        cpu.backlearning(true);
      else
        cpu.backlearning(false);
      
    } else {
      std::cout<<"Draw game";
    }

    /* asks the user to continue. We accept just 'y' or 'n' */
    char cntn = 'x';

    while (cntn!='y' && cntn!='n') {
      std::cout<<"Continue (y/n)?";
      std::cin>>cntn;
    }

    playing = (cntn == 'y');
  }

  // if user wants to quit, print game over and final score
  std::cout<<"=== Game Over ==="<<std::endl;

  //...and free the j33matrix space
  for (int i=0; i<3; i++)
    delete board[i];
  delete board;
}
