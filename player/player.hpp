
#ifndef __PLAYER__
#define __PLAYER__ 1

#include <map>
#include <memory>
#include <string>
#include <vector>

static const uint8_t CPU_ID = 101;

// By now, the Tic-Tac-Toe board is just a 2D array.
using j33matrix = uint8_t**;

struct Point{
  uint8_t x,y;
};

/**
 * Player is the base-class for all playing entities on Tic-Tac-Toe. It just
 * contains a reference to the board and informations about the player. It 
 * requires its derived class to implement the virtual method move.
 */
class Player{
protected:
  uint8_t ID;
  std::string name;
  j33matrix board_buffer;

public:
  Player(uint8_t ID, std::string name, j33matrix board);
  uint8_t get_ID();
  std::string get_name();
  virtual Point move() = 0;
};

#endif
