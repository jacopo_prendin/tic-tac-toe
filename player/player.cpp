
#include "player.hpp"

Player::Player(uint8_t ID, std::string pname, j33matrix board){
  this->ID = ID;
  name = std::string(pname);
  board_buffer = board;
}

uint8_t Player::get_ID(){
  return ID;
}

std::string Player::get_name(){
  return std::string(name);
}
