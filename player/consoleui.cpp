
#include "consoleui.hpp"

ConsoleUI::ConsoleUI(uint8_t ID, std::string username, j33matrix board, char me, char cpu) :
  Player(ID,username,board){
  this->me = me;
  this->cpu = cpu;
}


/**
 * The ConsoleUI move is a interface asking the user to choose where to put its
 * next piece, according to the situation printed on video. The returned point
 * is composed by coordinates given by stdin
 */
Point ConsoleUI::move(){
  for (auto y = 0; y<3; y++) {
    for (auto x = 0; x<3; x++) {
      if (board_buffer[x][y] == ID) std::cout<<me;
      else if (board_buffer[x][y] == CPU_ID) std::cout<<cpu;
      else std::cout<<'.';
    }
    std::cout<<std::endl;
  }

  // I keep the check of input limited to range [0:3[. 
  int ux = -1;
  int uy = -1;

  while ( ux<0 || ux>=3 || uy<0 || uy>=3 ) {
    std::cout<<"\n\nWhere do you want to move (row column)? ";
    std::cin>>ux;
    std::cin>>uy;
  }

  Point p;
  p.x = ux;
  p.y = uy;
  return p;
}
