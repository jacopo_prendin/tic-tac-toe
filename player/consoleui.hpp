

#ifndef __CONSOLEUI__
#define __CONSOLEUI__ 1

#include <iostream>
#include "player.hpp"

/**
 * Text-Based user interface to allow humans to play
 */
class ConsoleUI: public Player {
protected:
  char me;   /**< character to print to visualize human's pieces on board */
  char cpu;  /**< character to print to visualize opponent's pieces on board */
  
public:
  ConsoleUI(uint8_t ID, std::string username, j33matrix board, char me, char cpu);
  Point move();
};

#endif
