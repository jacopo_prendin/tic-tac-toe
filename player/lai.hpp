
#ifndef __LEARNING_AI__
#define __LEARNING_AI__ 1

#include <algorithm>
#include <iostream>
#include <exception>
#include <list>
#include <map>
#include <memory>

#include "player.hpp"

/**
 * Move is a container to next moves. It contains a score that LearningAI will
 * update and a position on the board. When the LearningAI wants to play the 
 * next move for a certain schema, it will look for the move with highest weigth
 */
class Move{
private:
  Point p;
  int16_t weigth;

public:
  Move(uint8_t x, uint8_t y, int16_t weigth=0);
  Point get_point();
  void add_to_weigth(uint16_t delta);
  int16_t get_weigth();
};

/*
 * A "memlist" is a list of moves. Using shared pointers the whole declaration
 * becomes too long and I decided to use an alias
 */
using memlist=std::list<std::shared_ptr<Move>>;
using memlist_ptr = std::shared_ptr<memlist>;
// ------------------------------- LEARNING AI --------------------------------
std::string hash(j33matrix board);

/**
 * The learning AI player
 */
class LearningAI: public Player {
protected:
  /* temp_memory saves the moves during a match, to improve the score at
   * the end. permanent_memory store Move(s) and it's being consulted during
   * a match to find the best Move to executed
   */
  std::map<std::string, memlist_ptr> permanent_memory;
  std::map<std::string, memlist_ptr> temp_memory; 
  
public:
  LearningAI(j33matrix board);
  ~LearningAI();

  void backlearning(bool winner);
  Point move();
};

#endif
