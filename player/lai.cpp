
#include "lai.hpp"

// ---------------------------------- Move ------------------------------------
/**
 * Move constructor.
 */
Move::Move(uint8_t x, uint8_t y, int16_t weigth){
  p.x = x;
  p.y = y;
  this->weigth = weigth;
}


Point Move::get_point() {
  return p;
}

void Move::add_to_weigth(uint16_t delta){
  weigth += delta;
}

int16_t Move::get_weigth() {
  return weigth;
}


// ------------------------------- LearningAI ---------------------------------
/**
 * Implmenentation is really simple: just transform the board status in a
 * array on 9 elements.
 */
std::string hash(j33matrix board){
  auto hash = std::string(64,'X');
  for (int x=0; x<3; x++)
    for (int y=0; y<3; y++)
      hash[x+y] = board[x][y];

  return hash;
}

/**
 * Constructor
 */
LearningAI::LearningAI(j33matrix board) :
  Player(CPU_ID,"CPU", board){
}

LearningAI::~LearningAI(){
  std::cout<<"LearningAI Destroyed";
}
/**
 * this function is used on LearningAI->move method to find the highest scored
 * move in the list
 */
bool compare_moves(std::shared_ptr<Move> one,
                   std::shared_ptr<Move> two){
  return one->get_weigth() < two->get_weigth();
}

Point LearningAI::move(){
  std::string status = hash(board_buffer);
  Point choosen_point;

  // if exist a key equal to status
  if (permanent_memory.find(status) != permanent_memory.end() ) {
    // locate the move with highest score for this board status
    auto status_list = permanent_memory[status];
    std::shared_ptr<Move> best = *std::max_element(status_list->begin(),
                                              status_list->end(),
                                              compare_moves);

    /*
     * save this move on temp_memory. If match will ends with winning,
     * move's score will increase.
     */

    /* if we have already this status on temporary memory let's use it; if no,
     * first create a new list
     */
    memlist_ptr t;
    if (temp_memory.find(status) != temp_memory.end() ) {
      t = temp_memory[status];
    } else {
      t = std::make_shared<memlist>(memlist());
    }
    t->push_back(best);
    choosen_point = best->get_point();

  } else {
    /*
     * this status hasn't been examinated yet: create a Move object with the
     * first free cell, create a new list of moves ad save the combination
     * <status,list> on temp_memory
     */
    for (uint8_t x = 0; x<3; x++) {
      for (uint8_t y = 0; y<3; y++) {
        if (board_buffer[x][y] == 0) {
          auto newmove = std::make_shared<Move>(x,y);
          memlist_ptr moves = std::make_shared<memlist>(memlist());
          moves->push_back(newmove);
          temp_memory.insert(std::make_pair(status,moves) );
          choosen_point = newmove->get_point();
        } // loop y
      } // loop x
    } // managing exception
  } //exception

  return choosen_point;
}

/**
 * backlearning
 */
void LearningAI::backlearning(bool winner){

  // for each status in temp_memory
  for(auto it = temp_memory.begin();
      it != temp_memory.end(); it++) {
    auto status = it->first;
    for (auto l = temp_memory[status]->begin();
         l != temp_memory[status]->end();
         l++) {

      auto move= *l;
      /*
       * if it's a win situation, add +1 to score of each move, else
       * subtract one. 
       */
      if (winner) move->add_to_weigth(1);
      else move->add_to_weigth(-1);
    } //for-each Move

    /*
     * use this memlist to replace the one stored in permanent list
     * with the same status-key
     */
    permanent_memory[status] = temp_memory[status];
  } // for each key
     
  // clean temp_memory
  temp_memory.clear();
}
