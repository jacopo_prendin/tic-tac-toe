
/**
 * The TicTacToe object manages the game, deciding who have to play, detecting
 * winner and draw-game situations
 */
#include "player/player.hpp"

using Playerp = Player*;
  
class TicTacToe {
private:
  j33matrix board; /**< the tictactoe board. */

  // situation is easy enough to do not require smart pointers
  Playerp current; /**< pointer to player that can move */
  Playerp next;    /**< pointer to player that will move */
  Playerp winner;  /**< pointer to winning player. Used just to print its name
                    * and usually set to NULL
                    */

  /**
   * Method used to set the winner according to its ID
   */
  void set_winner(int ID);

  // Check methods
  bool vertical_winner(uint8_t column);
  bool horizontal_winner(uint8_t row);
  bool diangonal_sx_winner();
  bool diangonal_dx_winner();

public:
  TicTacToe(j33matrix board, Player* a, Player* b);

  void reset();

  bool match_running();

  void manage_player_next_move();

  bool is_draw_game();

  std::string get_winner();
};
