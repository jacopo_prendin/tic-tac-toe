
#include "../player/lai.hpp"

#include <string.h>
#include <assert.h>

LearningAI get_test_ai(){
  j33matrix board = new uint8_t*[3];
  for (auto i=0; i<3; i++) {
    board[i] = new uint8_t[3];
    memset(board[i],0,3);
  }

  // put some values
  board[0][0] = 1;
  board[2][2] = CPU_ID;
  board[1][1] = 1;
  return LearningAI(board);
}

/**
 * try a simple initialization of an empty LearningAI object
 */
void test_initialization() {
  auto test1 = get_test_ai();

  assert(test1.get_ID() == CPU_ID); 
}


int main(int argc, char** argv){
  test_initialization();
}
