
#include "../player/consoleui.hpp"

ConsoleUI get_test_user(){
  j33matrix board = new uint8_t*[3];
  for (auto i=0; i<3; i++) {
    board[i] = new uint8_t[3];
    memset(board[i],0,3);
  }

  // put some values
  board[0][0] = 1;
  board[2][2] = CPU_ID;
  board[1][1] = 1;
  return ConsoleUI(1,std::string("Test User"),board, 'X', 'O');
}

void test_userinput() {
  auto testuser = get_test_user();

  std::cout<<"Expected Board output right here"<<std::endl;
  auto p = testuser.move();
  std::cout<<"You wrote "<<(int)p.x<<","<<(int)p.y<<std::endl;
}

int main(){
  test_userinput();
}
