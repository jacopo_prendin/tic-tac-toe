

#include "tictactoe.hpp"
// ----------------------------- Private Methods -------------------------------

/**
 * just sets the pointer to the winning player, according to its ID
 */
void TicTacToe::set_winner(int ID) {
  if (ID == current->get_ID())
    winner = current;
  else
    winner = next;
}

/**
 * Checks if someone is winning with a vertical line
 */
bool TicTacToe::vertical_winner(uint8_t column){
  if (column<0 || column>2) return false;

  bool result = true;
  auto id = board[column][0];
  if (id == 0) return false;

  for (int row=0; row<3; row++) {
    result = result && (id == board[column][row]);
  }
  if (result) set_winner(id);
  return result;
}

/**
 * Checks if someone is winning with a horizontal line
 */
bool TicTacToe::horizontal_winner(uint8_t row){
  if (row<0 || row>2) return false;

  bool result = true;
  auto id = board[0][row];
  if (id == 0) return false;

  for (int column=0; column<3; column++) {
    result = result && (id == board[column][row]);
  }
  if (result) set_winner(id);
  return result;
}

/**
 * Checks if someone is winning with line up-sx to down-dx
 */
bool TicTacToe::diangonal_sx_winner(){
  bool result = true;
  auto id = board[0][0];
  if (id == 0) return false;

  // row and columns have the same value on the diagonal
  for (int i=0; i<3; i++) {
    result = result && (id == board[i][i]);
    }
  }
  if (result) set_winner(id);
  return result;
}

/**
 * Checks if someone is winning with line up-dx to down-sx
 */
bool TicTacToe::diangonal_dx_winner(){
  bool result = true;
  auto id = board[0][2];
  if (id == 0) return false;

  // the diagonal from dx: if rows increase, columns decrease
  for (int i=0; i<3; i++) {
    result = result && (id == board[2-i][i]);
  }
  if (result) set_winner(id);
  return result;
}


// ----------------------------- Public Methods --------------------------------

/**
 * Constructor keeps track of the matrix board and of players
 */
TicTacToe::TicTacToe(j33matrix nboard, Player* a, Player* b){
  this->board = nboard;
  this->current = a;
  this->next = b;
  winner = NULL;
}

/**
 * reset the game setting to zero all board's cells
 */
void TicTacToe::reset(){

  winner = NULL;
  for (int x=0; x<3; x++)
    for (int y=0; y<3; y++)
      board[x][y] = 0;
}
/**
 * the method let the current player to give a position to place its next piece.
 * Until a valid position isn't given, we continue to query the player. On
 * next release we should introduce a timeout, or a max attempts number
 */
void TicTacToe::manage_player_next_move() {
  Point p = current->move();

  /* until the current player doesn't give a valid position, we continue to
   * query it for a valid position to place its piece
   */
  while (board[p.x][p.y] != 0) {
    p = current->move();
  }

  board[p.x][p.y] = current->get_ID();

  /* switch players! */
  Playerp tmp = current;
  current = next;
  next = tmp;
}

/**
 * Is the match still running (no winners, no draw game)?
 */
bool TicTacToe::match_running() {
  /* check for victory situations */
  for (auto i=0; i<3; i++) {
    // 1. verticals
    if (vertical_winner(i)) return false;

    // 2. horizontals
    if (horizontal_winner(i)) return false;
  }
  // 3. diagonals
  if (diangonal_sx_winner()) return false;
  if (diangonal_dx_winner()) return false;
  
  /* if all tiles have a piece, it's a draw game */
  if (is_draw_game()) return false;
  
  /* no winner, no draw-game. Match still on */
  return true;
}

/**
 * A draw game is when all cells have a piece
 */
bool TicTacToe::is_draw_game() {
  auto tot_zeros = 0;
  for (int x=0; x<3; x++) {
    for (int y=0; y<3; y++) {
      if (board[x][y] == 0)
        tot_zeros++;
    }
  }
  /* some cells are still empty */
  return (tot_zeros==0);
}

/**
 * returns winner's name. If no winner is available, returns an empty string
 */
std::string TicTacToe::get_winner() {
  if (winner) return winner->get_name();
  else return "";
}
